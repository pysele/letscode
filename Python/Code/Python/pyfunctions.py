# x = 20
# y = 30

# result = x + y  
# print(result)

# DRY - do not repeat yourself

# x = 40
# y = 30

# result = x + y
# print(result)


# x = 100
# y = 150

# result = x + y
# print(result)


# x = 100
# y = 200

# result = x + y
# print(result)


# x = 100
# y = 300

# result = x + y
# print(result)

def addition():  # No input No output
	x = 20;
	y = 30;
	result = x + y
	#print(result)

addition()

def addition():  # No input yes output
	x = 20;
	y = 30;
	result = x + y
	print(result)
	return result

addition()



def addition(num1, num2):    # parameters  # yes input No output

	x = num1   # hard coding
	y = num2
	result = x + y
	result = x - y 
	print("Adding 2 nums - num1 & num2 = " + str(result))
	return result

addition(30, 40)
z = addition(30, 40)

def addition_return(num1, num2):    # parameters  # Yes input Yes output
	x = num1   # hard coding 
	y = num2
	result = x + y  
	print("Adding 2 nums - num1 & num2 = " + str(result))
	return result

z = addition_return(50, 50)
print(z)

#addition(20, 30)  # arguments

# def addition_4030():
# 	x = 40
# 	y = 30
# 	result = x + y  
# 	print(result)

# addition_4030()

#addition(40, 30)


# def addition_100150():
# 	x = 100
# 	y = 150
# 	result = x + y  
# 	print(result)

# addition_100150()

#addition(100, 150)


# Define function to calculate area of circle

# r = 5 
# r = 10
# r = 7


def calculate_circle_area(radius):
	"""
	This function helps to calculate the area of circle
	:param- radius
	:ptype- float

	:return - area
	:rtype- float
	"""
	pi = 3.14
	r = radius
	area = pi * r * r
	return area
	#print("The area of cicle = " + str(area))  # str + number(decimal/integer)  str(integer)

calculate_circle_area(5)
calculate_circle_area(10)
calculate_circle_area(7)

calculate_circle_area.__doc__



#return type

def calculate_avg(num1, num2):
	result = addition_return(num1, num2)
	avg = result/2
	print("The average of 2 nums = " + str(avg))

calculate_avg(20, 30)


def function1(niwm ):
	return output






















