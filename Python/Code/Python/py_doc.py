
# print(len.__doc__)

# print(str.__doc__)

def calculate_circle_area(radius):
	"""Helps to calculate area of a circle
	Args:
    	radius (float): radius of a circle
	"""
	pi = 3.14
	r = radius
	area = pi * r * r


def calculate_circle_area_with_return1(radius):
	"""Helps to calculate area of a circle

	:param radius: radius of circle
	:type radius: float
	
	:returns: area of circle
	:rtype: float
	"""
	pi = 3.14
	r = radius
	area = pi * r * r
	return area



def calculate_circle_area_with_return2(radius):
	"""Helps to calculate area of a circle
	Args:
    	radius (float): radius of a circle
    Returns:
    	area (float): area of circle
	"""
	pi = 3.14
	r = radius
	area = pi * r * r
	return area



print(calculate_circle_area.__doc__)

print(calculate_circle_area_with_return1.__doc__)


print(calculate_circle_area_with_return2.__doc__)


print(help(calculate_circle_area))

print(help(calculate_circle_area_with_return1))






