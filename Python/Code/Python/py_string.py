# ## To define string

random_string = 'This is a session on python'

another_string = "Its awesome to be here with you all"

new_string_one = "It's awesome to be here with you all"

new_string = 'It\'s awesome to be here with you all'   #  \escape chracter

new_string_two = "It\"s awesome to be here with you all"

new_string_two = 'It"s awesome to be here with you all'

# # print(new_string)

# # print(new_string_two)

new_string_two = "HELLO Guys !!" # H-0 E-1 L-2 L-3


print(new_string_two[4])  # H

# # To find the length of string
len(new_string_two)



print(len(new_string_two)) 

# new_string_two[0]

# # # To access O
print(new_string_two[4])

# # Slicing of string
print(new_string_two[0:8])   # including 0 excluding 4
print(new_string_two[:8]) 

print(new_string_two[2:13])

# print(new_string_two[3:len(new_string_two)])

print(new_string_two[2:])

# print(new_string_two[0:5])

# -5 -4  -3  -2 -1
# x = H E L L O
# 0 1 2 3 4
# x[0:3]
# x[-5:-2]




print(new_string_two[-1])

print(new_string_two[-4:-1])

# # print(new_string_two[-1:-4])

# # Changes the cases -

new_string_two = "hELLO guys !!"

new_string_title = "hELLO Hello guys !!" # Hello Guys !! HELLO guys !!

print("The lower() function for " +new_string_two+' '+new_string_two.lower()) # hello guys !!
print("The upper() function for " +new_string_two+' '+new_string_two.upper()) # HELLO GUYS !!
print("The title() function for " +new_string_title+' '+new_string_title.title())  ## it will convert the first letter of every word within any string in upper case
print("The capitalize() function for " +new_string_title+' '+new_string_title.capitalize())  # it will convert the first letter of any string in upper case


# 

hi_string = new_string_title.replace('Hello', 'Hi')
print(hi_string)

# #
L_count = new_string_title.count('L') 
print(L_count)
l_count = new_string_title.count('l')
print(l_count)


# # 
name_list = "Mr. John Doe"

split_string = name_list.split()
print(split_string)

split_string = name_list.split('.')
print(split_string)


# # 
space_string = 'John Doe'
print(len(space_string)) # 8

space_string_two = ' John Doe '
print(len(space_string_two)) # 10

space_string_strip = space_string_two.strip()
print(len(space_string_strip)) # 8 


# #
# #print(dir(space_string_strip))

# # 

print(space_string.find('John')) # 0 - 14

print(space_strin.find('z'))


# 
print(space_string.index('J'))
print(space_string.index('Doe'))

# print(space_string[:-1])

# print(space_string)





