
# Binary Operators  - two operands
# Arithmetic Operators : + - / % *
# Relational operators : > , < , >= , <=, ==
# Assignment = 
x = 4
y = x



# Unary Operators  - single operands
# Increment ++,  2++  , 
# Decrement --, 2--
# 	1
#    ____
# 4 | 4
# 	 4
# 	___
# 	0
# Ternary Operators - 3 operands

print(x + y)
print(x - y)

print(x * y)

print(x / y) # 
print(x % y) # modulus / mod

# 
print(x > y) # False
print(x < y) # False

print(x >= y) # True
print(x <= y) # True

print(x == y) # True




# Augmented Assignment Operator
x+=2
x = x+2
x+=1
print(x)  # 5 


x = x -1
x -= 1
# print(x--) # 4
print(x)

x = x /2

x /=2
print(x)
x = x *2 
x *= 2

print("Hello" * 3)
print("Hello" + str(3))

print("Hello %f.2" % 3.14)










