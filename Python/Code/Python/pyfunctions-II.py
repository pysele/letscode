def area_of_circle(radius):  # yes input return no output
	PI = 3.14
	area = PI * radius * radius

z = area_of_circle(5)
print(z)
area_of_circle(5.5)

def area_of_circle_return(radius):  # yes input yes return output
	PI = 3.14
	area = PI * radius * radius
	return area

z = area_of_circle_return(5)
print(z)
area_of_circle_return(5.5)