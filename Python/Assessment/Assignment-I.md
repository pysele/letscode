#### ASSIGNMENT - II

**_HTML & CSS_**

1. Design HTML document to display your details as follows:

![RESUME](lw00002110.png)

1. Add styling to HTML file using `<style>` tag

2. At last add style to HTML using `style.css` file

> Note: The content on the webpage should be at the center from left & right
> 
> Use google font - https://fonts.google.com/specimen/Raleway
> 
> Use similar font colors as shown in above image
> 
> Replace the content of above image with yours in html document

**_PYTHON_**

1. Explain the concept of _Compilation_ & _Interpretation_?

2. What are _Variables_? Explain with example.

3. What are _Functions_? Explain with example.

4. Design a python code that helps to show following statements on screen as a output

    `- Hello All, Its time to learn Python`

    `- Hey, Python is a fun !!`

    `- Python is named after a comedian series - Monthy Python.`

5. Design a Python code to calculate area of circle having radius `r = 3.5`

6. Convert the above code into Python function and test the same

7. Design a Python code to convert temperature from degree Celcius to Fahrenheit and vice versa


**_Happy Coding & Testing !!_**


