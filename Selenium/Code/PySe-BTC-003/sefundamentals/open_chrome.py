from selenium import webdriver

# Creating instance of chrome driver
chrome_driver = webdriver.Chrome("../drivers/chromedriver")
# Open url
chrome_driver.get("https://www.selenium.dev/")
# To get the tile of page
print("Title of page is :" + chrome_driver.title)
# Closing the browser
chrome_driver.close()
# Quitting the chromedriver
chrome_driver.quit()


# Creating instance of firefox driver
firefox_driver = webdriver.Firefox("../drivers/geckodriver")
# Open url
firefox_driver.get("https://www.selenium.dev/")
# To get the tile of page
print("Title of page is :" + firefox_driver.title)
# Closing the browser
firefox_driver.close()
# Quitting the chromedriver
firefox_driver.quit()

