from time import sleep

from selenium import webdriver

driver = webdriver.Chrome("../drivers/chromedriver")
driver.get("https://www.amazon.in")

# Get the title
homepage_title = driver.title
print(homepage_title)

# Find the location of searchbox
searchbox = driver.find_element_by_xpath("//input[@aria-label='Search']")
searchbox.send_keys("Mobiles")

searchicon = driver.find_element_by_xpath("//input[@value='Go']")
searchicon.click()

sleep(3)

# searchbox = driver.find_element_by_xpath("//input[@aria-label='Search']")
# searchbox.clear()
# searchbox.send_keys("Macbook pro")
# searchicon = driver.find_element_by_xpath("(//input[@type='submit'])[1]")
# searchicon.click()

search_item_1 = driver.find_element_by_xpath("(//a[@class='a-link-normal a-text-normal'])[1]")
print(search_item_1.text)

search_item_2 = driver.find_element_by_xpath("(//a[@class='a-link-normal a-text-normal'])[2]/span")
print(search_item_2.text)


sleep(4)

# Closing the browser
driver.close()
# Quitting the chromedriver
driver.quit()
