# LIST

# List - its a data structure in python that helps to store group of elements in a sequential manner.
# Every element in list will have its own position(index) and to access elements from list we can use this index


# Create list/ define list

# Blank list
programming_languages = []
print(programming_languages)

programming_languages = ['Python', 'Java', 'C++']
print(programming_languages)

random_list = ['Python', 'Java', 'C++', 23.67, True, 34536]
#                  0       1       2      3    4       5
print(random_list)

# Indexing always start from 0
first_ele = random_list[0]
print("I am at first position " + first_ele)

sec_ele = random_list[1]
print("I am at second position " + sec_ele)

last_ele = random_list[-1]
print("I am at last position " + str(last_ele))

# To get the length of list
length_of_list = len(random_list)
print(length_of_list)

# To add new elements to the list
random_list.append("Ruby")

print(random_list)

# To add new item in between list
random_list.insert(0, 125.78)

print(random_list)


