# ID
# NAME
# CLASSNAME
# LINKTEXT
# PARTIAL_LINK_TEXT
# XPATH
# CSS
# TAGNAME

from time import sleep

from selenium import webdriver

# Creating instance of chrome driver
driver = webdriver.Chrome("../drivers/chromedriver")
# Open url
driver.get("https://www.selenium.dev/")
# To get the tile of page
print("Title of page is :" + driver.title)

# Tell the location of downloads to selenium
downloads_element = driver.find_element_by_link_text("Downloads")

# downloads_element = driver.find_element_by_partial_link_text("Downlo")

# Tell Selenium to perform click operation
downloads_element.click()

# To perform click operation on grid documentation
grid_documentation_link = driver.find_element_by_link_text("documentation")

grid_documentation_link.click()

grid_page_serchbox = driver.find_element_by_id("search-by")
grid_page_serchbox.send_keys("Webdriver")

sleep(3)

# grid_page_close_icon = driver.find_element_by_class_name("fas fa-times")
# grid_page_close_icon = driver.find_element_by_xpath("//html/body/nav/div/div[2]/span")
# grid_page_close_icon.click()

# Relative xpath - //div[@class='searchbox']/span
grid_page_close_icon = driver.find_element_by_xpath("//div[@class='searchbox']/span")
grid_page_close_icon.click()

# //tagname[@attribute_name='attribute_value']
# //h2[@id='webdriver']

# //parent_tagname[@attribute_name='attribute_value']/child_tagname

# //tagname[text()='text_value']
# //p[text()='It drives the browser effectively.']

# //tagname[contains(text(), 'partial_text')]
#//p[contains(text(),'WebDriver is designed as')]


getting_started = driver.find_element_by_partial_link_text("Getting started")
getting_started.click()

webdriver_link = driver.find_element_by_link_text("WebDriver")
webdriver_link.click()


webdriver_heading = driver.find_element_by_xpath("//h1[@id='webdriver']")
print(webdriver_heading.text)

para1_text = driver.find_element_by_xpath("//div[@id='body-inner']/p[1]")
print(para1_text.text)

para2_text = driver.find_element_by_xpath("//div[@id='body-inner']/p[2]")
print(para2_text.text)

para3_text = driver.find_element_by_xpath("//div[@id='body-inner']/p[3]")
print(para3_text.text)



# Tell the location of searchbox to selenium using locator NAME
# searchbox_element = driver.find_element_by_name("search")

# Tell selenium to perform enter text operation
# searchbox_element.send_keys("Selenium")

sleep(4)

# Closing the browser
driver.close()
# Quitting the chromedriver
driver.quit()





# Absolute xpath - starts from root node or tag of HTML document (html)

# //html/body/nav/div/div[2]/span